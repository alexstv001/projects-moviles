object Singleton{

    init {
        println("Invocación a la clase Singleton")
    }
    var variableName = "Variable 1"
    fun printVarName(){
        println(variableName)
    }

}

fun main(args: Array<String>) {
    Singleton.printVarName()
    Singleton.variableName = "Nueva variable"

    var a = A()
}

class A {

    init {
        println("Método de inicio de clase. Propiedad Singleton variableName : ${Singleton.variableName}")
        Singleton.printVarName()
    }
}