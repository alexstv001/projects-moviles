//Patron de diseño Decorador
import org.junit.jupiter.api.Test

interface Cafetera {
    fun hacerCafePequeño()
    fun hacerCafeGrande()
}

class MaquinaCafeteraNormal : Cafetera {
    override fun hacerCafePequeño() = println("Normal: hacer café pequeño")

    override fun hacerCafeGrande() = println("Normal: hacer café grande")
}

//Decorador:
class cafeteraMejorada(private val cafetera: Cafetera) : Cafetera by cafetera {

    // comportamiento dominante
    override fun hacerCafeGrande() {
        println("Mejorado: hacer café grande")
    }

    // comportamiento extendido
    fun hacerCafeConLeche() {
        println("Mejorado: Hacer café con leche")
        cafetera.hacerCafePequeño()
        addMilk()
    }

    private fun addMilk() {
        println("Mejorado: Agregar leche")
    }
}

class DecoratorTest {

    @Test
    fun Decorator() {
        val normalMachine = MaquinaCafeteraNormal()
        val enhancedMachine = cafeteraMejorada(normalMachine)

        // non-overridden behaviour
        enhancedMachine.hacerCafePequeño()
        // overridden behaviour
        enhancedMachine.hacerCafeGrande()
        // extended behaviour
        enhancedMachine.hacerCafeConLeche()
    }
}