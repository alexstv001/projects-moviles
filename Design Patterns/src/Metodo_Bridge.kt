interface Switch {
    var dispositivo: dispositivo
    fun encender()
}
interface dispositivo {
    fun run()
}
class ControlRemoto(override var dispositivo: dispositivo) : Switch {
    override fun encender() = dispositivo.run()
}
class TV : dispositivo {
    override fun run() = println("TV encendida")
}
class aspiradora : dispositivo {
    override fun run() = println("Aspiradora encendida")
}
fun main(args: Array<String>) {
    var tvRemoteControl = ControlRemoto(dispositivo = TV())
    tvRemoteControl.encender()
    var aspiradoraRemoteControl = ControlRemoto(dispositivo = aspiradora())
    aspiradoraRemoteControl.encender()
}