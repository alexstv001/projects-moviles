////////////////////////////////////////////////////////////////////////////////////
/////////////  Nombre: Alex Steeve Carrillo Criollo  ///////////////////////////////
/////////////  Curso: GR1                            ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

interface InterfaceMoneda { //Interfaz de moneda con dos funciones: símbolo y código
    fun simbolo_Moneda(): String
    fun codigo_Moneda(): String
}

class Euro : InterfaceMoneda { //Clase Euro que proporciona cuerpo a las funciones
    override fun simbolo_Moneda(): String {
        return "€"
    }
    override fun codigo_Moneda(): String {
        return "EUR"
    }
}

class Dolar : InterfaceMoneda { //Clase Dolar que proporciona cuerpo a las funciones
    override fun simbolo_Moneda(): String {
        return "$"
    }
    override fun codigo_Moneda(): String {
        return "USD"
    }
}

enum class Pais { //Lista de paises a elegir
    EstadosUnidos, Italia, Ecuador, Alemania
}

fun moneda(country: Pais): InterfaceMoneda? {
    when (country) { //Constructor "when" para elegir qué objeto usar
        Pais.Italia, Pais.Alemania -> return Euro()
        Pais.EstadosUnidos -> return Dolar()
        else -> return null
    }
}

fun main(args: Array<String>) { //Llamar a la función "moneda" para crear el objeto en función del país.
    val noCurrencyCode = "\"Este país no posee moneda propia\""

    println(moneda(Pais.Alemania)?.codigo_Moneda() ?: noCurrencyCode)
    println(moneda(Pais.Italia)?.codigo_Moneda() ?: noCurrencyCode)
    println(moneda(Pais.EstadosUnidos)?.codigo_Moneda() ?: noCurrencyCode)
    println(moneda(Pais.Ecuador)?.codigo_Moneda() ?: noCurrencyCode)
}