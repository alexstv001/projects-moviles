import java.util.*

class Burger(val name: String)
class Bob : Observable() {
    val name = "Bob"
    fun cookBurger(name : String){
        var burger = Burger(name)

        // Llamar a setChanges () antes de llamar a notifyObservers ()
        setChanged() //Heredado desde Observable ()
        notifyObservers(burger) //Heredado desde Observable()
    }
}

class Tina : Observer{
    val name = "Tina"
    override fun update(o: Observable?, arg: Any?) {
        when (o){
            is Bob -> {
                if (arg is Burger){
                    println("$name esta sirviendo el  ${arg.name} cocinado por ${o.name}")
                }
            }
            else -> println(o?.javaClass.toString())
        }
    }
}

fun main(args : Array<String>){
    val bob = Bob()
    bob.addObserver(Tina())
    bob.cookBurger("It takes Bun to Know Bun Burger")
}