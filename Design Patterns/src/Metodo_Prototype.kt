data class EMail(var receptor: String, var asunto: String?, var mensaje: String?) {
    fun quote(startIndex: Int) : String {
        return "> ${mensaje?.substring(startIndex)}"
    }
}

fun main(args: Array<String>) {
    val mail = EMail("abc@example.com", "Correo de prueba", "Este es un mensaje de prueba")

    val copy = mail.copy(receptor = "other@example.com")

    println("Email1 va a " + mail.receptor + " con asunto " + mail.asunto)
    println("Email2 va a " + copy.receptor + " con asunto " + copy.asunto)
}
