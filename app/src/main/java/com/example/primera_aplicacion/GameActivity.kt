package com.example.primera_aplicacion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class GameActivity : AppCompatActivity() {

    private lateinit var activityTitle : TextView
    private lateinit var playerName : String
    private lateinit var gamescoreTextView : TextView
    private lateinit var timeLeftTextView :TextView
    private lateinit var tapMeButton : Button

    private lateinit var countDownTimer : CountDownTimer
    private var initialCountDown : Long = 10000
    private var countDownInterval : Long = 1000

    private var timeLeft = 10
    private var gameScore = 0
    private var isGameStarted = false

    //crear una variable para que guarde el nombre del activity en el que estamos
    private val TAG = GameActivity::class.java.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        Log.d(TAG, "onCreate called, score is $gameScore")


        activityTitle = findViewById(R.id.game_title)
        gamescoreTextView = findViewById(R.id.score_text_view)
        timeLeftTextView = findViewById(R.id.time_text_view)
        tapMeButton= findViewById(R.id.tap_me_button)

        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "ERROR"
        activityTitle.text = getString(R.string.get_ready_player,playerName)

        tapMeButton.setOnClickListener {it.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce))
            incrementScore()}


        if (savedInstanceState !=null){
            gameScore = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(SCORE_KEY, gameScore)
        outState.putInt(TIME_LEFT_KEY, timeLeft)
        outState.putBoolean(GAME_STARTED, isGameStarted)

        //si ya tengo la variable timeleft, el contador debe detenerse

        countDownTimer.cancel()
        Log.d(TAG, "onSaveInstanceState: Saving score: $gameScore and timeleft: $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        countDownTimer.cancel()
        Log.d(TAG, "onDestroy called")
    }

    private fun incrementScore(){

        if(!isGameStarted){
            startGame()
        }
        gameScore ++
        gamescoreTextView.text = getString(R.string.score, gameScore)
    }

    private fun resetGame(){
        gameScore = 0
        gamescoreTextView.text = getString(R.string.score, gameScore)

        timeLeft = 10
        timeLeftTextView.text = getString(R.string.time_left, timeLeft)

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft)
            }
            override fun onFinish() {
                endGame()
            }
        }

        isGameStarted = false
    }

    private fun startGame(){
        countDownTimer.start()
        isGameStarted = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()
    }

    private fun restoreGame(){
        gamescoreTextView.text = getString(R.string.score, gameScore)
        timeLeftTextView.text = getString(R.string.time_left, timeLeft)

        configCountDownTimer()


        if (isGameStarted){
            countDownTimer.start()
        }

    }

    private fun configCountDownTimer(){
        countDownTimer = object : CountDownTimer((timeLeft * 1000).toLong(), countDownInterval){
            override fun onFinish() {
                endGame() //To change body of created functions use File | Settings | File Templates.
            }

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text = getString(R.string.time_left, timeLeft)

            }
        }
    }


    //como no tenemos la variable static, COMPANION crea constantes que van a almacenar nombres de las variables que voy a utilizar
    companion object{
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private const val GAME_STARTED = "GAME_STARTED"
    }
}
